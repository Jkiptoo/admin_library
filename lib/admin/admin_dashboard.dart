// ignore_for_file: must_be_immutable

import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:myapp/Dashboard/home_page.dart';
import 'package:myapp/Lib/borrow_book.dart';
import 'package:myapp/Lib/return_book.dart';
import 'package:myapp/fetch/admin_latest_book.dart';
import 'package:myapp/fetch/books_in_library.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AdminDashboard extends StatefulWidget {
  String? userId;
  AdminDashboard({
    Key? key,
    this.userId,
  }) : super(key: key);

  @override
  State<AdminDashboard> createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  SharedPreferences? adminData;
  String? adminEmail;
  File? _image;
  final imagePicker = ImagePicker();
  String? downloadUrl;
  Future imagePickerMethod() async {
    final pick =
        // ignore: invalid_use_of_visible_for_testing_member
        await ImagePicker.platform.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pick != null) {
        _image = File(pick.path);
      } else {
        showSnackBar('No File Selected', const Duration(milliseconds: 400));
      }
    });
  }

  Future uploadImage() async {
    final postID = DateTime.now().millisecondsSinceEpoch.toString();
    Reference ref = FirebaseStorage.instance
        .ref()
        .child('${widget.userId}user_image')
        .child('post_$postID');
    await ref.putFile(_image!);
    downloadUrl = await ref.getDownloadURL();
    // ignore: avoid_print
    print(downloadUrl);
  }

  showSnackBar(String snackText, Duration d) {
    final snackBar = SnackBar(
      content: Text(snackText),
      duration: d,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
    initial();
  }

  void initial() async {
    adminData = await SharedPreferences.getInstance();
    setState(() {
      adminEmail = adminData!.getString('adminEmail');
    });
  }
  Widget buildDrawer(){
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        radius: 55,
                        child: ClipOval(
                          child: SizedBox(
                            width: 110,
                            height: 110,
                            child: (_image != null)
                                ? Image.file(
                              _image!,
                              fit: BoxFit.fill,
                            )
                                : Image.asset('asset/extra.jpg'),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 60),
                      child: IconButton(
                          onPressed: () {
                            imagePickerMethod();
                            uploadImage();
                          },
                          icon: const Icon(
                            Icons.camera_alt_outlined,
                            size: 30,
                          )),
                    )
                  ],
                ),
                Text(
                  '$adminEmail',
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                )
              ],
            ),
            decoration: const BoxDecoration(),
          ),
          ListTile(
            title: const Text('Book Library'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const BooksInLibrary()));
            },
          ),
          ListTile(
            title: const Text('Issue Book'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const IssueBook()));
            },
          ),
          ListTile(
            title: const Text('Return Book'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ReturnBook()));
            },
          ),
          ListTile(
            title: const Text('Lost Book'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Latest Books'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const AdminPageLatest()));
            },
          ),
          ListTile(
            title: const Text('About'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
  Widget buildBottomNavigation(){
    return  BottomNavigationBar(items: <BottomNavigationBarItem>[
      BottomNavigationBarItem(
          label: '',
          icon: Material(
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const SplashScreen()));
              },
              child: const Image(
                  height: 30, image: AssetImage('asset/home.png')),
            ),
          )),
      BottomNavigationBarItem(
          label: '',
          icon: Material(
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const BooksInLibrary()));
              },
              child: const Image(
                  height: 30, image: AssetImage('asset/library.png')),
            ),
          )),
      BottomNavigationBarItem(
        label: '',
        icon: Material(
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child:
            const Image(height: 30, image: AssetImage('asset/add.png')),
          ),
        ),
      ),
    ]);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: buildDrawer(),
      appBar: AppBar(
        title: const Text('Admin DashBoard'),
        centerTitle: true,
        backgroundColor: Colors.teal[800],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 20,
            ),
            Column(
              children: <Widget>[
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: <Widget>[
                      const SizedBox(
                        width: 25,
                      ),
                      Material(
                        child: InkWell(
                          onTap: () {},
                          child: Column(
                            children: const <Widget>[
                              Text(
                                '300',
                                style: TextStyle(
                                    fontSize: 25, fontWeight: FontWeight.bold),
                              ),
                              Text('All Books',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.pinkAccent))
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 50,
                      ),
                      Material(
                        child: InkWell(
                          onTap: () {},
                          child: Column(
                            children: const <Widget>[
                              Text('30',
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold)),
                              Text('Books Out',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 50,
                      ),
                      Material(
                        child: InkWell(
                          onTap: () {},
                          child: Column(
                            children: const <Widget>[
                              Text('3',
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold)),
                              Text('Lost Books',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.green))
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const Image(
                    fit: BoxFit.fitWidth,
                    image: AssetImage('asset/dashboard.png'))
              ],
            ),
            Row(
              children: const <Widget>[
                SizedBox(
                  width: 30,
                ),
                Text(
                  'Borrowing History',
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),
            const SizedBox(
              height: 70,
            ),
          ],
        ),
      ),
      bottomNavigationBar: buildBottomNavigation()
    );
  }
}
