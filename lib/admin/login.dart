import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:myapp/admin/admin_dashboard.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AdminLogin extends StatefulWidget {
  const AdminLogin({Key? key,}) : super(key: key);

  @override
  State<AdminLogin> createState() => _AdminLoginState();
}

class _AdminLoginState extends State<AdminLogin> {
  // ignore: unused_field
  String? _email;
  final _emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  SharedPreferences? adminData;
  bool? newUser;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkIfUSerIsLogedIn();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Form(
          key: _formKey,
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Image.asset('asset/admins.png'),
                TextFormField(
                  controller: _emailController,
                  onSaved: (value) {
                    _emailController.text = value!;
                  },
                  onChanged: (val) {
                    setState(() {
                      _email = val;
                    });
                  },
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[\w-]+@([\w-]+\.)+[\w-]{2,4}')
                            .hasMatch(value)) {
                      return 'Enter a Valid Email';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      hintText: 'Email',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  obscureText: true,
                  validator: (value) {
                    String missings = '';
                    if (value!.length < 8) {
                      missings += "Password has at least 8 characters\n";
                    }

                    if (!RegExp("(?=.*[a-z])").hasMatch(value)) {
                      missings +=
                          "Password must contain at least one lowercase letter\n";
                    }
                    if (!RegExp("(?=.*[A-Z])").hasMatch(value)) {
                      missings +=
                          "Password must contain at least one uppercase letter\n";
                    }
                    if (!RegExp((r'\d')).hasMatch(value)) {
                      missings += "Password must contain at least one digit\n";
                    }
                    if (!RegExp((r'\W')).hasMatch(value)) {
                      missings += "Password must contain at least one symbol\n";
                    }

                    //if there is password input errors return error string
                    if (missings != "") {
                      return missings;
                    } else {
                      return null;
                    }
                  },
                  decoration: InputDecoration(
                      hintText: 'Password',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        String adminEmail = _emailController.text;
                        if (adminEmail != '') {
                          // ignore: avoid_print
                          print('Successful');
                          adminData!.setString('adminEmail', adminEmail);
                        }
                        FirebaseAuth.instance
                            .signInWithEmailAndPassword(
                                email: 'levis@gmail.com', password: 'Levis254#')
                            .then((value) => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AdminDashboard())));
                        adminData!.setString('adminEmail', adminEmail);
                      }
                    },
                    child: const Text('Login'))
              ],
            ),
          )),
    ));
  }

  void checkIfUSerIsLogedIn() async {
    adminData = await SharedPreferences.getInstance();
    newUser = (adminData!.getBool('register') ?? true);
  }
}
