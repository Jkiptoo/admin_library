// ignore_for_file: must_be_immutable

import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:myapp/Dashboard/home_page.dart';
import 'package:myapp/Lib/make_note.dart';
import 'package:myapp/Screens/books_page.dart';
import 'package:myapp/fetch/book.dart';
import 'package:myapp/fetch/books_in_library.dart';
import 'package:myapp/fetch/latest_books_data.dart';
import 'package:myapp/home/about_book.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyHomePage extends StatefulWidget {
  String? userId;
  MyHomePage({
    Key? key,
    this.userId,
  }) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  SharedPreferences? loginData;
  String? useremail;
  File? _image;
  final imagePicker = ImagePicker();
  String? downloadUrl;
  Future imagePickerMethod() async {
    final pick =
        // ignore: invalid_use_of_visible_for_testing_member
        await ImagePicker.platform.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pick != null) {
        _image = File(pick.path);
      } else {
        showSnackBar('No File Selected', const Duration(milliseconds: 400));
      }
    });
  }

  Future uploadImage() async {
    final postID = DateTime.now().millisecondsSinceEpoch.toString();
    Reference ref = FirebaseStorage.instance
        .ref()
        .child('${widget.userId}user_image')
        .child('post_$postID');
    await ref.putFile(_image!);
    downloadUrl = await ref.getDownloadURL();
    // ignore: avoid_print
    print(downloadUrl);
  }

  showSnackBar(String snackText, Duration d) {
    final snackBar = SnackBar(
      content: Text(snackText),
      duration: d,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
    countBooks();
    lostBookCount();
    booksOutCount();
    initial();
  }

  void initial() async {
    loginData = await SharedPreferences.getInstance();
    setState(() {
      useremail = loginData!.getString('useremail');
    });
  }

  int bookCount = 0;
  countBooks() async {
    QuerySnapshot allBook =
        await FirebaseFirestore.instance.collection('bookCollection').get();
    List<DocumentSnapshot> allBooks = allBook.docs;
    bookCount = allBooks.length;
    setState(() {
      bookCount = allBooks.length;
    });
  }

  int lostBook = 0;
  lostBookCount() async {
    QuerySnapshot lostBookData =
        await FirebaseFirestore.instance.collection('Lost').get();
    List<DocumentSnapshot> lostData = lostBookData.docs;
    lostBook = lostData.length;
    setState(() {
      lostBook = lostData.length;
    });
  }

  int booksOutNum = 0;
  booksOutCount() async {
    QuerySnapshot bookOut =
        await FirebaseFirestore.instance.collection('BooksOut').get();
    List<DocumentSnapshot> bookOut2 = bookOut.docs;
    booksOutNum = bookOut2.length;
    setState(() {
      booksOutNum = bookOut2.length;
    });
  }

  bool isBookReturned = false;
  checkBookReturned() async {
    // isBookReturned = FirebaseFirestore.instance.collection('collectionPath').where(field).get()
  }
  var userUID = FirebaseAuth.instance.currentUser!.uid;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My DashBoard'),
        centerTitle: true,
        backgroundColor: Colors.teal[800],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 20,
            ),
            Column(
              children: <Widget>[
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: <Widget>[
                      const SizedBox(
                        width: 25,
                      ),
                      Material(
                        child: InkWell(
                          onTap: () {},
                          child: Column(
                            children: <Widget>[
                              Text(
                                '$bookCount',
                                style: const TextStyle(
                                    fontSize: 25, fontWeight: FontWeight.bold),
                              ),
                              const Text('All Books',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.pinkAccent))
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 50,
                      ),
                      Material(
                        child: InkWell(
                          onTap: () {},
                          child: Column(
                            children: <Widget>[
                              Text('$booksOutNum',
                                  style: const TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold)),
                              const Text('Books Out',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 50,
                      ),
                      Material(
                        child: InkWell(
                          onTap: () {},
                          child: Column(
                            children: <Widget>[
                              Text('$lostBook',
                                  style: const TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold)),
                              const Text('Lost Books',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.green))
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const Image(
                    fit: BoxFit.fitWidth,
                    image: AssetImage('asset/dashboard.png')),
                Row(
                  children: const <Widget>[
                    SizedBox(
                      width: 30,
                    ),
                    Text(
                      'Latest Books',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                StreamBuilder<QuerySnapshot>(
                  stream:
                  FirebaseFirestore.instance.collection('Latest').snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    return (snapshot.connectionState == ConnectionState.waiting)
                        ? const Center(
                      child: CircularProgressIndicator(),
                    )
                        : SizedBox(
                      height: 1000,
                      child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: snapshot.data!.docs.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot doc = snapshot.data!.docs[index];
                            return  Card(
                              elevation: 10,
                              child: ListTile(
                                title: Text(doc['title']),
                                subtitle: Text(doc['author']),
                                trailing: Text(doc['code']),
                              ),
                            );
                          }),
                    );
                  },
                ),
              ],
            ),

            const SizedBox(
              height: 70,
            ),

          ],
        ),
      ),
      bottomNavigationBar:  BottomNavigationBar(items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            label: '',
            icon: Material(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SplashScreen()));
                },
                child: const Image(
                    height: 30, image: AssetImage('asset/home.png')),
              ),
            )),
        BottomNavigationBarItem(
            label: '',
            icon: Material(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const BooksInLibrary()));
                },
                child: const Image(
                    height: 30, image: AssetImage('asset/library.png')),
              ),
            )),
        BottomNavigationBarItem(
          label: '',
          icon: Material(
            child: InkWell(
              onTap: () {
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>const MakeNotes()));
              },
              child: const Image(
                  height: 30, image: AssetImage('asset/add.png')),
            ),
          ),
        ),
      ]),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: CircleAvatar(
                          radius: 55,
                          child: ClipOval(
                            child: SizedBox(
                              width: 110,
                              height: 110,
                              child: (_image != null)
                                  ? Image.file(
                                      _image!,
                                      fit: BoxFit.fill,
                                    )
                                  : Image.asset('asset/extra.jpg'),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 60),
                        child: IconButton(
                            onPressed: () {
                              imagePickerMethod();
                              uploadImage();
                            },
                            icon: const Icon(
                              Icons.camera_alt_outlined,
                              size: 30,
                            )),
                      )
                    ],
                  ),
                  Text(
                    '$useremail',
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              decoration: const BoxDecoration(),
            ),
            ListTile(
              title: const Text('Book Library'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const BooksPage()));
              },
            ),
            ListTile(
                title: GestureDetector(
              child: const Text('Book'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const FetchBooksData()));
              },
            )),
            ListTile(
              title: const Text('Latest Books'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const LatestBooksData()));
              },
            ),
            ListTile(
              title: const Text('Make Notes'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const MakeNotes()));
              },
            ),
            ListTile(
              title: const Text('About'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AboutInformation()));
              },
            ),
            ListTile(
              title: const Text('Log Out'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const SplashScreen()));
              },
            ),
          ],
        ),
      ),
    );
  }
}

// Container(
//   padding: const EdgeInsets.all(0),
//   height: 105,
//   child: Card(
//     child: Row(
//       children: <Widget>[
//         const SizedBox(
//           width: 30,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: const <Widget>[
//             Text('Issued to ', style: TextStyle(fontSize: 7)),
//             Text('Date of Issue', style: TextStyle(fontSize: 7)),
//             Text('Book Code', style: TextStyle(fontSize: 7)),
//             Text('Title', style: TextStyle(fontSize: 7)),
//             Text('Author', style: TextStyle(fontSize: 7)),
//             Text('Notes', style: TextStyle(fontSize: 7)),
//             Text('Status', style: TextStyle(fontSize: 7))
//           ],
//         ),
//         const SizedBox(
//           width: 20,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             const Text('', style: TextStyle(fontSize: 7)),
//             const Text('31-11-2019', style: TextStyle(fontSize: 7)),
//             const Text('KJM-278', style: TextStyle(fontSize: 7)),
//             const Text('Chase The Lion',
//                 style: TextStyle(fontSize: 7)),
//             const Text('Mark Batterson',
//                 style: TextStyle(fontSize: 7)),
//             const Text('Book issued in good condition',
//                 style: TextStyle(fontSize: 7)),
//             ButtonTheme(
//               minWidth: 50,
//               height: 5,
//               child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                     backgroundColor: Colors.orange,
//                   ),
//                   onPressed: () {},
//                   child: const Text('OverDue')),
//             )
//           ],
//         ),
//         const SizedBox(
//           width: 70,
//         ),
//         const Image(image: AssetImage('asset/extra.jpg'))
//       ],
//     ),
//   ),
// ),
// Container(
//   padding: const EdgeInsets.all(0),
//   height: 105,
//   child: Card(
//     child: Row(
//       children: <Widget>[
//         const SizedBox(
//           width: 30,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: const <Widget>[
//             Text(
//               'Issued ',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Date of Issue',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Book ID',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Title',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Author',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Notes',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Status',
//               style: TextStyle(fontSize: 7),
//             )
//           ],
//         ),
//         const SizedBox(
//           width: 20,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             const Text(
//               'Charity Wayua',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               '31-11-2019',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'KJM-278',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Chase The Lion',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Mark Batterson',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Book issued in good condition',
//               style: TextStyle(fontSize: 7),
//             ),
//             ButtonTheme(
//               minWidth: 50,
//               height: 5,
//               child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                     backgroundColor: Colors.lightGreen,
//                   ),
//                   onPressed: () {},
//                   child: const Text('Returned')),
//             )
//           ],
//         ),
//         const SizedBox(
//           width: 70,
//         ),
//         const Image(image: AssetImage('asset/mee.jpg'))
//       ],
//     ),
//   ),
// ),
// Container(
//   padding: const EdgeInsets.all(0),
//   height: 105,
//   child: Card(
//     child: Row(
//       children: <Widget>[
//         const SizedBox(
//           width: 30,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: const <Widget>[
//             Text(
//               'Issued to',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Date of Issue',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Book ID',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Title',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Author',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Notes',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Status',
//               style: TextStyle(fontSize: 7),
//             )
//           ],
//         ),
//         const SizedBox(
//           width: 20,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             const Text(
//               'Charity Wayua',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               '31-11-2019',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'KJM-278',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Chase The Lion',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Mark Batterson',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Book issued in good condition',
//               style: TextStyle(fontSize: 7),
//             ),
//             ButtonTheme(
//               minWidth: 50,
//               height: 5,
//               child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                     backgroundColor: Colors.orange,
//                   ),
//                   onPressed: () {},
//                   child: const Text('OverDue')),
//             )
//           ],
//         ),
//         const SizedBox(
//           width: 70,
//         ),
//         const Image(image: AssetImage('asset/extra.jpg'))
//       ],
//     ),
//   ),
// ),
// Container(
//   padding: const EdgeInsets.all(0),
//   height: 105,
//   child: Card(
//     child: Row(
//       children: <Widget>[
//         const SizedBox(
//           width: 30,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: const <Widget>[
//             Text(
//               'Issued to',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Date of Issue',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Book ID',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Title',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Author',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Notes',
//               style: TextStyle(fontSize: 7),
//             ),
//             Text(
//               'Status',
//               style: TextStyle(fontSize: 7),
//             )
//           ],
//         ),
//         const SizedBox(
//           width: 20,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             const Text(
//               'Charity Wayua',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               '31-11-2019',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'KJM-278',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Chase The Lion',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Mark Batterson',
//               style: TextStyle(fontSize: 7),
//             ),
//             const Text(
//               'Book issued in good condition',
//               style: TextStyle(fontSize: 7),
//             ),
//             ButtonTheme(
//               minWidth: 50,
//               height: 2,
//               child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                     backgroundColor: Colors.orange,
//                   ),
//                   onPressed: () {},
//                   child: const Text('OverDue')),
//             )
//           ],
//         ),
//         const SizedBox(
//           width: 70,
//         ),
//         const Image(image: AssetImage('asset/extra.jpg'))
//       ],
//     ),
//   ),
// ),