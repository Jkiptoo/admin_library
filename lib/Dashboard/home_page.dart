import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';
import 'package:myapp/home/sign_up.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: IntroductionScreen(
      animationDuration: 3,
      pages: [
        PageViewModel(
            title: 'KIBOMET LIB APP',
            body: 'USER SIDE',
            image: buildLottie('asset/json/99349-girl-with-books.json'),
            decoration: pageDeco()),
        PageViewModel(
            title: 'Community based library',
            body: 'Enlightening the community',
            image: buildLottie('asset/json/123752-start-up-meeting.json'),
            decoration: pageDeco()),
        PageViewModel(
            title: 'Books collection',
            body: 'We cover you with the best books',
            image: buildLottie(
              'asset/json/123652-financial-charts-and-statistics-on-tab-with-up-arrow.json',
            ),
            decoration: pageDeco()),
        PageViewModel(
            title: 'Register',
            body: 'Register today to access the vast resource in our libraries',
            image: buildLottie('asset/json/125886-login-bounce.json'),
            decoration: pageDeco())
      ],
      next: const Icon(Icons.arrow_forward),
      done: Text(
        'Explore',
        style: TextStyle(
            color: Colors.teal[900], fontSize: 24, fontWeight: FontWeight.bold),
      ),
      onDone: () {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => const SignUp()));
      },
    ));
  }
}

Widget buildLottie(String path) {
  return Center(
    child: Lottie.asset(path),
  );
}

Widget buildImage(String path) {
  return Center(
    child: Image.asset(path),
  );
}

PageDecoration pageDeco() {
  return PageDecoration(
      titleTextStyle: TextStyle(
          color: Colors.teal[900], fontSize: 32, fontWeight: FontWeight.bold),
      bodyPadding: const EdgeInsets.all(20),
      imagePadding: const EdgeInsets.all(24),
      bodyTextStyle: TextStyle(
          color: Colors.grey[700], fontSize: 25, fontWeight: FontWeight.w600));
}
