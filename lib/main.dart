import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myapp/Dashboard/home_page.dart';
import 'package:myapp/Lib/return_book.dart';
import 'package:myapp/fetch/fetch_book.dart';
import 'package:myapp/home/reset_file.dart';
import 'package:myapp/home/sign_in.dart';
import 'package:myapp/home/sign_up.dart';
import 'package:myapp/Dashboard/book_lib.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: const SplashScreen(),
      routes: {
        '/login/': (context) => const SignInPage(),
        '/library/': (context) => MyHomePage(),
        '/reset/': (context)=> const ResetPassword(),
        '/fetch/': (context)=> const FetchData(),
        '/return/': (context)=> const ReturnBook(),
        '/home/': (context)=> const SplashScreen(),
        '/register/': (context)=> const SignUp()
      },
    );
  }
}
