import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class BooksPage extends StatefulWidget {
  const BooksPage({Key? key}) : super(key: key);

  @override
  State<BooksPage> createState() => _BooksPageState();
}

class _BooksPageState extends State<BooksPage> {
  final searchController = TextEditingController();
  final database = FirebaseFirestore.instance;
  String? searchString;
  // uploadData(String title) async {
  //   List<String> splitList = title.split(' ');
  //   List<String> indexList = [];
  //
  //   for (int i = 0; i < splitList.length; i++) {
  //     for (int j = 0; j < splitList[i].length + i; j++) {
  //       indexList.add(splitList[i].substring(0, j).toLowerCase());
  //     }
  //   }
  //   database
  //       .collection('bookCollection')
  //       .add({'book': title, 'searchIndex': indexList});
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/library/', (route) => false);
              },
              icon: const Icon(
                Icons.arrow_back,
                size: 25,
              )),
          title: const Text('Search Book'),
          centerTitle: true,
          backgroundColor: Colors.teal[900],
        ),
        body: Column(
          children: [
            Expanded(
                child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.all(20),
                  child: TextFormField(
                    onChanged: (val) {
                      setState(() {
                        searchString = val.toLowerCase();
                      });
                    },
                    controller: searchController,
                    decoration: InputDecoration(
                        hintText: 'Search Books Here',
                        suffixIcon: IconButton(
                            onPressed: () {
                              searchController.clear();
                            },
                            icon: const Icon(Icons.clear)),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                ),
                Expanded(
                    child: StreamBuilder<QuerySnapshot>(
                  stream: (searchString == null || searchString!.trim() == '')
                      ? FirebaseFirestore.instance
                          .collection('bookCollection')
                          .snapshots()
                      : FirebaseFirestore.instance
                          .collection('bookCollection')
                          .where('searchIndex', arrayContains: searchString)
                          .snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Text('We have an Error ${snapshot.error}');
                    }
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                        return const SizedBox(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      case ConnectionState.none:
                        return const Text('Oops No Data Found');
                      case ConnectionState.active:
                        // TODO: Handle this case.
                        break;
                      case ConnectionState.done:
                        // TODO: Handle this case.
                        return const Text('We are Done!');
                    }
                    return ListView.builder(
                        itemCount: snapshot.data!.docs.length,
                        itemBuilder: (context, index) {
                          DocumentSnapshot doc = snapshot.data!.docs[index];
                          return ListTile(
                            title: Text(doc['title']),
                          );
                        });
                  },
                ))
              ],
            )),
          ],
        ));
  }
}

// child: StreamBuilder<QuerySnapshot>(
//     stream: FirebaseFirestore.instance.collection('books').snapshots(),
//     builder: (context, snapshot) {
//       return (snapshot.connectionState == ConnectionState.waiting)
//           ? const Center(
//         child: CircularProgressIndicator(),
//       )
//           : ListView.separated(
//         itemCount: snapshot.data!.docs.length,
//         itemBuilder: (context, index) {
//           DocumentSnapshot data = snapshot.data!.docs[index];
//           return ListTile(
//               title: Text(data['title'],style:const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
//               subtitle: Text(data['author']),
//               trailing:  IconButton(onPressed: (){}, icon:const Icon(
//                 Icons.add_shopping_cart,
//                 size: 30,
//                 color: Colors.indigo,
//               ),)
//           );
//         },
//         separatorBuilder: (context, index) {
//           return const Divider();
//         },
//       );
//     }),
