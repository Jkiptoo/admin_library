import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myapp/Dashboard/book_lib.dart';
import 'package:myapp/Lib/make_note.dart';
import 'package:myapp/Make_changes/edit_notes.dart';


class FetchNotes extends StatefulWidget {
  const FetchNotes({Key? key}) : super(key: key);

  @override
  State<FetchNotes> createState() => _FetchNotesState();
}

class _FetchNotesState extends State<FetchNotes> {
  CollectionReference fetchBook =
  FirebaseFirestore.instance.collection('Notes');
  Future<void> deleteNote(id) async {
    fetchBook
        .doc(id)
        .delete()
        .then((value) => ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Note deleted successfully'))))
        .catchError((error) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Something went Wrong: $error')));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyHomePage()));
            },
            icon: const Icon(Icons.arrow_back)),
        title: const Text('Note Collection'),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance.collection('Notes').snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              const CircularProgressIndicator();
            }
            final List storeFetchBooks = [];
            snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
              Map c = documentSnapshot.data() as Map<String, dynamic>;
              storeFetchBooks.add(c);
              c['id'] = documentSnapshot.id;
            }).toList();
            return ListView(children: [
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      for (var i = 0; i < storeFetchBooks.length; i++) ...[
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text('Note:'),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(storeFetchBooks[i]['note']),
                                const SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                IconButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => UpdateNote(
                                                  id: storeFetchBooks[i]
                                                  ['id'])));
                                    },
                                    icon: const Icon(
                                      Icons.edit_note,
                                      size: 50,
                                    ))
                              ],
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                IconButton(
                                    onPressed: () {
                                      deleteNote(storeFetchBooks[i]['id']);
                                    },
                                    icon: const Icon(
                                      Icons.delete,
                                      size: 50,
                                    ))
                              ],
                            )
                          ],
                        )
                      ]
                    ],
                  ),
                ),
              ),
            ]);
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const MakeNotes()));
        },
        backgroundColor: Colors.teal[900],
        child: const Icon(
          Icons.add,
          size: 30,
        ),
      ),
    );
  }
}
