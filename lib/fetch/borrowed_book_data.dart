import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myapp/Lib/return_book.dart';
import 'package:myapp/admin/admin_dashboard.dart';

class BorrowedBookData extends StatefulWidget {
  const BorrowedBookData({Key? key}) : super(key: key);

  @override
  State<BorrowedBookData> createState() => _BorrowedBookDataState();
}

class _BorrowedBookDataState extends State<BorrowedBookData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AdminDashboard()));
            },
            icon: const Icon(Icons.arrow_back)),
        title: const Text('Issued Books'),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('BooksOut').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return const Text('There is no Book in database');
          }
          return ListView(
            children: getBooksData(snapshot),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal[900],
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const ReturnBook()));
        },
        child: const Icon(
          Icons.add,
          size: 30,
        ),
      ),
    );
  }

  getBooksData(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data!.docs
        .map(
          (doc) => Container(
        padding: const EdgeInsets.all(5),
        height: 102,
        child: Card(
          child: Row(
            children: <Widget>[
              const SizedBox(
                width: 30,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const <Widget>[
                  Text('User ', style: TextStyle(fontSize: 10)),
                  Text('Date Of Issue ', style: TextStyle(fontSize: 10)),
                  Text('Date of Return', style: TextStyle(fontSize: 10)),
                  Text('Book Code', style: TextStyle(fontSize: 10)),
                  Text('Book Title', style: TextStyle(fontSize: 10)),
                  Text('Book Author', style: TextStyle(fontSize: 10)),
                  Text('Book Copies', style: TextStyle(fontSize: 10)),
                ],
              ),
              const SizedBox(
                width: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(doc['User'], style: const TextStyle(fontSize: 10)),
                  Text(doc['issue'],
                      style: const TextStyle(fontSize: 10)),
                  Text(doc['return'],
                      style: const TextStyle(fontSize: 10)),
                  Text(doc['bookCode'],
                      style: const TextStyle(fontSize: 10)),
                  Text(doc['Title'],
                      style: const TextStyle(fontSize: 10)),
                  Text(doc['Author'],
                      style: const TextStyle(fontSize: 10)),
                  Text(doc['copies'],
                      style: const TextStyle(fontSize: 10)),
                ],
              ),
              const SizedBox(
                width: 25,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    width: 20,
                  ),
                  IconButton(
                    icon: const Icon(
                      Icons.edit,
                      size: 30,
                    ),
                    onPressed: () {},
                  ),
                ],
              ),
              const SizedBox(
                width: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    width: 20,
                  ),
                  IconButton(
                    icon: const Icon(
                      Icons.delete,
                      size: 30,
                    ),
                    onPressed: () {},
                  )
                ],
              )
            ],
          ),
        ),
      ),
    )
        .toList();
  }
}
