import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class LatestBooksData extends StatefulWidget {
  const LatestBooksData({Key? key}) : super(key: key);

  @override
  State<LatestBooksData> createState() => _LatestBooksDataState();
}

class _LatestBooksDataState extends State<LatestBooksData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        leading: IconButton(
            onPressed: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil('/library/', (route) => false);
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
            )),
        title: const Text('Latest Books'),
        centerTitle: true,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('Latest').snapshots(),
        builder: (context, snapshot) {
          return (snapshot.connectionState == ConnectionState.waiting)
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    DocumentSnapshot data = snapshot.data!.docs[index];
                    return Container(
                      padding: const EdgeInsets.all(5),
                      child: Container(
                        padding: const EdgeInsets.all(0),
                        child: Card(
                          elevation: 1,
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(data['title']),
                                subtitle: Text(data['author']),
                                trailing: Text(data['code']),
                              )
                            ],
                          ),
                        ),
                      ));
                  });
        },
      ),
    );
  }
}
