import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myapp/Dashboard/book_lib.dart';

class FetchBooksData extends StatefulWidget {
  const FetchBooksData({Key? key}) : super(key: key);

  @override
  State<FetchBooksData> createState() => _FetchBooksDataState();
}

class _FetchBooksDataState extends State<FetchBooksData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyHomePage()));
            },
            icon: const Icon(Icons.arrow_back)),
        title: const Text('Book Collection'),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance.collection('books').snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return const Text('No Data found');
            }
            return ListView(
              children: getAddBookData(snapshot),
            );
          }),
    );
  }

  getAddBookData(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data!.docs
        .map((doc) => ListTile(
              title: Text(doc['title']),
              subtitle: Text(doc['category']),
              trailing: Text(doc['author']),
            ))
        .toList();
  }
}
