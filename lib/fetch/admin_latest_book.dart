import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myapp/admin/admin_dashboard.dart';

class AdminPageLatest extends StatefulWidget {
  const AdminPageLatest({Key? key}) : super(key: key);

  @override
  State<AdminPageLatest> createState() => _AdminPageLatestState();
}

class _AdminPageLatestState extends State<AdminPageLatest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AdminDashboard()));
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
            )),
        title: const Text('Latest Books'),
        centerTitle: true,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('Latest').snapshots(),
        builder: (context, snapshot) {
          return (snapshot.connectionState == ConnectionState.waiting)
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    DocumentSnapshot data = snapshot.data!.docs[index];
                    return Container(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Book Title :',
                                style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    fontSize: 20,
                                    color: Colors.grey[800]),
                              ),
                              Text('Book Author:',
                                  style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontSize: 20,
                                      color: Colors.grey[800])),
                            ],
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(data['title'],
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.grey[700])),
                              Text(data['author'],
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.grey[700])),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.edit,
                                    size: 30,
                                  )),
                            ],
                          ),
                          IconButton(
                              onPressed: () {}, icon: const Icon(Icons.delete))
                        ],
                      ),
                    );
                  });
        },
      ),
    );
  }
}
