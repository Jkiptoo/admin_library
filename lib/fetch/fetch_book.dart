import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myapp/admin/admin_dashboard.dart';

class FetchData extends StatefulWidget {
  const FetchData({Key? key}) : super(key: key);

  @override
  State<FetchData> createState() => _FetchDataState();
}

class _FetchDataState extends State<FetchData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AdminDashboard()));
            },
            icon: const Icon(Icons.arrow_back)),
        title: const Text('RETURNED BOOKS'),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('BookData').snapshots(),
        builder: (context, snapshot) {
          return (snapshot.connectionState == ConnectionState.waiting)
              ? const CircularProgressIndicator()
              : ListView.separated(
                  itemBuilder: (context, index) {
                    DocumentSnapshot data = snapshot.data!.docs[index];
                    return Container(
                        padding: const EdgeInsets.all(20),
                        child: Card(
                          child: Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text('Title'),
                                  Text('Author'),
                                  Text('Code'),
                                  Text('User'),
                                  Text('Return'),
                                ],
                              ),
                              const SizedBox(
                                width: 30,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(data['bookTitle']),
                                  Text(data['bookAuthor']),
                                  Text(data['bookCode']),
                                  Text(data['user']),
                                  Text(data['dateOfReturn']),
                                ],
                              ),
                              const SizedBox(
                                width: 30,
                              ),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.edit_note,
                                    size: 50,
                                  )),
                              const SizedBox(
                                width: 30,
                              ),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.delete,
                                    size: 50,
                                  )),
                            ],
                          ),
                        ));
                  },
                  separatorBuilder: (context, index) {
                    return const Divider();
                  },
                  itemCount: snapshot.data!.docs.length);
        },
      ),
    );
  }
}
