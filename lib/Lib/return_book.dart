import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myapp/admin/admin_dashboard.dart';
import 'package:myapp/fetch/fetch_book.dart';

class ReturnBook extends StatefulWidget {
  const ReturnBook({
    Key? key,
  }) : super(key: key);

  @override
  State<ReturnBook> createState() => _ReturnBookState();
}

class _ReturnBookState extends State<ReturnBook> {
  final DateTime _time = DateTime.now();
  final DateFormat _dateFormatter = DateFormat('MMM dd yyyy');
  _handleDatePicker() async {
    final DateTime? date = await showDatePicker(
        context: context,
        initialDate: _time,
        firstDate: DateTime(2022),
        lastDate: DateTime(2040));
    if (date != null && date != _time) {
      setState(() {
        date;
      });
      _dateController.text = _dateFormatter.format(date);
    }
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _userController = TextEditingController();
  final _titleController = TextEditingController();
  final _authorController = TextEditingController();
  final _bookCodeController = TextEditingController();
  final _dateController = TextEditingController();
  void clearText() {
    _userController.clear();
    _titleController.clear();
    _authorController.clear();
    _bookCodeController.clear();
    _dateController.clear();
  }

  String? myEmail;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(
                  width: 20,
                ),
                FloatingActionButton(
                  backgroundColor: Colors.deepPurpleAccent,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AdminDashboard()));
                  },
                  child: const Icon(
                    Icons.arrow_back,
                    size: 30,
                  ),
                )
              ],
            ),
            Container(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CircleAvatar(
                    backgroundImage: AssetImage('asset/returned.png'),
                    radius: 100,
                    child: ClipOval(),
                  )
                ],
              ),
            ),
            Form(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        onSaved: (value) {
                          _userController.text = value!;
                        },
                        controller: _userController,
                        validator: (value) {
                          if (value!.isEmpty ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter a valid user';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'User',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        onSaved: (value) {
                          _titleController.text = value!;
                        },
                        validator: (value) {
                          if (value!.isEmpty ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter a Valid Book Title';
                          }
                          return null;
                        },
                        controller: _titleController,
                        decoration: InputDecoration(
                            labelText: 'Book Title',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        onSaved: (value) {
                          _authorController.text = value!;
                        },
                        validator: (value) {
                          if (value!.isEmpty ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter valid Book Author';
                          }
                          return null;
                        },
                        controller: _authorController,
                        decoration: InputDecoration(
                            labelText: 'Book Author',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        onSaved: (value) {
                          _bookCodeController.text = value!;
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Date is Needed';
                          }
                          return null;
                        },
                        controller: _bookCodeController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: 'Book Code',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        onSaved: (value) {
                          _dateController.text = value!;
                        },
                        readOnly: true,
                        onTap: _handleDatePicker,
                        controller: _dateController,
                        decoration: InputDecoration(
                            labelText: 'Date of return',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                backgroundColor: Colors.teal),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                FirebaseFirestore.instance
                                    .collection('BookData')
                                    .doc()
                                    .set({
                                  'user': _userController.text,
                                  'bookTitle': _titleController.text,
                                  'bookAuthor': _authorController.text,
                                  'bookCode': _bookCodeController.text,
                                  'dateOfReturn': _dateController.text
                                }).then((value) {
                                  clearText();
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const FetchData()));
                                });
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('Successful')));
                              }
                            },
                            child: const Text(
                              'Return Book',
                              style: TextStyle(fontSize: 20),
                            ))
                      ],
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
