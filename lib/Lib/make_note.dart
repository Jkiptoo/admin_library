import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:myapp/Dashboard/book_lib.dart';
import 'package:myapp/Make_changes/edit_note.dart';
import 'package:myapp/fetch/fetch_notes.dart';

class MakeNotes extends StatefulWidget {
  const MakeNotes({Key? key}) : super(key: key);

  @override
  State<MakeNotes> createState() => _MakeNotesState();
}

class _MakeNotesState extends State<MakeNotes> {
  final _formKey = GlobalKey<FormState>();
  final _noteController = TextEditingController();
  bool isLoading = false;

  var firebaseUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        centerTitle: true,
        elevation: 0,
        title: const Text('Make Notes'),
        leading: GestureDetector(
          onTap: (){
            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>MyHomePage()));
          },
          child: Icon(Icons.arrow_back, size: 25, color: Colors.grey[300],),
        ),
      ),
      body: SingleChildScrollView(
          child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Form(
                key: _formKey,
                child: TextFormField(
                  onSaved: (val) {
                    _noteController.text = val!;
                  },
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                      return 'Please Enter Correct Text';
                    } else {
                      return null;
                    }
                  },
                  controller: _noteController,
                  keyboardType: TextInputType.text,
                  maxLines: 30,
                  decoration: InputDecoration(
                      hintText: 'Make notes while reading ..',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(2))),
                )),
            const SizedBox(
              height: 20,
            ),
            !isLoading
                ? Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const SizedBox(
                          width: 25,
                        ),
                        ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        const Color.fromRGBO(0, 64, 64, 80))),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                FirebaseFirestore.instance
                                    .collection('Notes')
                                    .doc()
                                    .set({
                                  'rid': firebaseUser?.uid,
                                  'note': _noteController.text,
                                }).then((value) => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const FetchNotes())));
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('Successful')));
                              }
                            },
                            child: const Text(
                              'Save',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            )),
                      ],
                    ),
                  )
                : const Center(
                    child: CircularProgressIndicator(),
                  )
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context)=>const EditNote()));
        },
        child: const Icon(Icons.edit_note, size: 40, ),
        backgroundColor: Colors.teal[900],
      ),
    );
  }
}
