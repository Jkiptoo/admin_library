import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myapp/admin/admin_dashboard.dart';
import 'package:myapp/fetch/latest_books_data.dart';

class LatestBook extends StatefulWidget {
  const LatestBook({Key? key}) : super(key: key);

  @override
  State<LatestBook> createState() => _LatestBookState();
}

class _LatestBookState extends State<LatestBook> {
  final _formKey = GlobalKey<FormState>();
  final _titleController = TextEditingController();
  final _authorController = TextEditingController();
  final _codeController = TextEditingController();
  final _descriptionController = TextEditingController();

  void clearText() {
    _titleController.clear();
    _authorController.clear();
    _codeController.clear();
    _descriptionController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AdminDashboard()));
          },
          icon: const Icon(
            Icons.arrow_back,
            size: 30,
          ),
        ),
        title: const Text('Latest Books'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  onSaved: (value) {
                    _titleController.text = value!;
                  },
                  controller: _titleController,
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                      return 'Enter Valid Book Title';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Book Title',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  onSaved: (value) {
                    _authorController.text = value!;
                  },
                  controller: _authorController,
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                      return 'Enter valid Author';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Book Author',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  onSaved: (value) {
                    _codeController.text = value!;
                  },
                  controller: _codeController,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Enter Valid Book Code';
                    }
                    if (value.length <= 5) {
                      return 'Book Code Requires at least five Characters';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Book Code',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _descriptionController,
                  onSaved: (value) {
                    _descriptionController.text = value!;
                  },
                  maxLines: 5,
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                      return 'Enter a valid Book Description';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Book Description',
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.teal[900],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5))),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        FirebaseFirestore.instance
                            .collection('Latest')
                            .doc()
                            .set({
                          'title': _titleController.text,
                          'author': _authorController.text,
                          'code': _codeController.text,
                          'description': _descriptionController.text
                        }).then((value) {
                          clearText();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const LatestBooksData()));
                        });
                      }
                    },
                    child: const Text(
                      'Submit',
                      style: TextStyle(fontSize: 20),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
