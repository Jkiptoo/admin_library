import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myapp/admin/admin_dashboard.dart';
import 'package:myapp/fetch/borrowed_book_data.dart';

class IssueBook extends StatefulWidget {
  const IssueBook({Key? key}) : super(key: key);

  @override
  State<IssueBook> createState() => _IssueBookState();
}

class _IssueBookState extends State<IssueBook> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final DateTime _date = DateTime.now();
  final TextEditingController _userController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _authorController = TextEditingController();
  final TextEditingController _codeController = TextEditingController();
  final TextEditingController _statusController = TextEditingController();
  final _returnController = TextEditingController();
  final _issueController = TextEditingController();
  final DateFormat _dateFormatter = DateFormat('MMM dd yyyy');
  _handleDatePicker() async {
    final DateTime? date = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2022),
      lastDate: DateTime(2050),
    );
    if (date != null && date != _date) {
      setState(() {
        date;
      });
      _returnController.text = _dateFormatter.format(date);
    }
  }

  final DateFormat _dateFormat = DateFormat('MMM dd yyyy');
  _handleDate() async {
    final DateTime? dates = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (dates != null) {
      setState(() {
        dates;
      });
      _issueController.text = _dateFormat.format(dates);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {});
  }

  void clearText() {
    _userController.clear();
    _issueController.clear();
    _titleController.clear();
    _authorController.clear();
    _codeController.clear();
    _statusController.clear();
    _returnController.clear();
    _copiesType.clear();
  }

  // ignore: prefer_typing_uninitialized_variables
  var selectedCopies;
  final List<String> _copiesType = <String>['1', '2', '3', '4'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          child: const Icon(Icons.arrow_back),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AdminDashboard()));
          },
        ),
        backgroundColor: Colors.teal[900],
        centerTitle: true,
        title: const Text(
          'BORROW\n BOOK',
          style: TextStyle(color: Colors.white),
          textAlign: TextAlign.center,
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Text(
                      'User',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'User is needed';
                        }
                        return null;
                      },
                      controller: _userController,
                      decoration: const InputDecoration(hintText: ''),
                      onSaved: (value) => setState(() {
                        _userController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Date of Issue',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Date is Needed';
                        }
                        return null;
                      },
                      readOnly: true,
                      controller: _issueController,
                      onTap: _handleDate,
                      decoration: const InputDecoration(),
                      onSaved: (value) => setState(() {
                        _issueController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Date Of Return',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Flexible(
                        child: TextFormField(
                      readOnly: true,
                      controller: _returnController,
                      onTap: _handleDatePicker,
                      decoration: const InputDecoration(),
                      onSaved: (value) {
                        setState(() {
                          _returnController.text = value!;
                        });
                      },
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Title',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Title is needed';
                        }
                        return null;
                      },
                      controller: _titleController,
                      decoration: const InputDecoration(hintText: ''),
                      onSaved: (value) => setState(() {
                        _titleController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Author',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Author is needed';
                        }
                        return null;
                      },
                      controller: _authorController,
                      decoration: const InputDecoration(hintText: ''),
                      onSaved: (value) => setState(() {
                        _authorController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Book Code',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Book Code is needed';
                        }
                        return null;
                      },
                      controller: _codeController,
                      decoration: const InputDecoration(hintText: ''),
                      onSaved: (value) => setState(() {
                        _codeController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Number of Copies',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: DropdownButton(
                      items: _copiesType
                          .map((value) => DropdownMenuItem(
                                child: Text(value),
                                value: value,
                              ))
                          .toList(),
                      onChanged: (selectedCopiesType) {
                        setState(() {
                          selectedCopies = selectedCopiesType;
                        });
                      },
                      value: selectedCopies,
                      isExpanded: true,
                      hint: const Text('Number of copies'),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.teal[900],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15))),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          FirebaseFirestore.instance
                              .collection('BooksOut')
                              .doc()
                              .set({
                            'User': _userController.text,
                            'issue': _issueController.text,
                            'Title': _titleController.text,
                            'Author': _authorController.text,
                            'bookCode': _codeController.text,
                            'return': _returnController.text,
                            'copies': selectedCopies.toString()
                          }).then((value) => {
                                    clearText(),
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const BorrowedBookData()))
                                  });
                          ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text('Processing Data')));
                        }
                      },
                      child: const Text(
                        'Save',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
