import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:myapp/services/users.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String? _email;
  String? _password;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  bool isLoading = false;
  bool hideText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(30),
                  height: 250,
                  child: const Image(
                    image: AssetImage('asset/jon.jpg'),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: TextFormField(
                    controller: _nameController,
                    onSaved: (value) {
                      _nameController.text = value!;
                    },
                    validator: (value) {
                      if (value!.isEmpty ||
                          !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                        return 'Please Enter Correct Name';
                      } else {
                        return null;
                      }
                    },
                    decoration: InputDecoration(
                        labelText: 'User',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: TextFormField(
                    controller: _emailController,
                    onSaved: (value) {
                      _emailController.text = value!;
                    },
                    onChanged: (value) {
                      setState(() {
                        _email = value;
                      });
                    },
                    validator: (value) {
                      if (value!.isEmpty ||
                          !RegExp(r'^[\w-]+@([\w-]+\.)+[\w-]{2,4}')
                              .hasMatch(value)) {
                        return 'Enter Valid Email';
                      } else {
                        return null;
                      }
                    },
                    decoration: InputDecoration(
                        labelText: 'Email',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: TextFormField(
                    controller: _phoneController,
                    onSaved: (val) {
                      _phoneController.text = val!;
                    },
                    keyboardType: TextInputType.phone,
                    validator: (value) {
                      if (value!.isEmpty ||
                          !RegExp(r'^(?:[+0]9)?[0-9]{10}$')
                              .hasMatch(value)) {
                        return 'Please enter a valid phone number';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        labelText: 'Phone Number',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: TextFormField(
                    onChanged: (value) {
                      setState(() {
                        _password = value;
                      });
                    },
                    validator: (value) {
                      String missings = '';
                      if (value!.length < 8) {
                        missings += "Password has at least 8 characters\n";
                      }

                      if (!RegExp("(?=.*[a-z])").hasMatch(value)) {
                        missings +=
                            "Password must contain at least one lowercase letter\n";
                      }
                      if (!RegExp("(?=.*[A-Z])").hasMatch(value)) {
                        missings +=
                            "Password must contain at least one uppercase letter\n";
                      }
                      if (!RegExp((r'\d')).hasMatch(value)) {
                        missings +=
                            "Password must contain at least one digit\n";
                      }
                      if (!RegExp((r'\W')).hasMatch(value)) {
                        missings +=
                            "Password must contain at least one symbol\n";
                      }

                      //if there is password input errors return error string
                      if (missings != "") {
                        return missings;
                      } else {
                        return null;
                      }
                    },
                    obscureText: hideText,
                    decoration: InputDecoration(
                        labelText: 'Password',
                        suffixIcon: GestureDetector(
                          onTap: (){
                            setState(() {
                              hideText = !hideText;
                            });
                            FocusScope.of(context).unfocus();
                          },
                          child: Icon(
                            hideText == false?
                                Icons.visibility:
                                Icons.visibility_off,
                            color: Colors.grey[800],
                          ),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                !isLoading?
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            backgroundColor: Colors.teal),
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            
                            FirebaseFirestore.instance
                                .collection('users')
                                .doc()
                                .set({
                              'user': _nameController.text,
                              'email': _emailController.text,
                              'phone': _phoneController.text,
                            });
                            FirebaseAuth.instance
                                .createUserWithEmailAndPassword(
                                    email: _email!, password: _password!)
                                .then((signedInUser) {
                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('User registered successfully',style: TextStyle(color: Colors.grey[400]),)));
                              UserManagement()
                                  .storeNewUser(signedInUser.user, context);
                            }).catchError((e) {
                              // ignore: avoid_print
                              showDialog(context: context, builder: (BuildContext context){
                                return AlertDialog(
                                  title: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text("Error", style: TextStyle(
                                        color: Colors.red[300],
                                        fontWeight: FontWeight.bold,
                                          fontSize: 20
                                      ),)
                                    ],
                                  ),
                                  content: Text('$e',style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 17
                                  ),),
                                  actions: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: Colors.teal[900],
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(2)
                                              )
                                            ),
                                            onPressed: (){Navigator.of(context).pop();}, child: Text('OK',style: TextStyle(
                                          color: Colors.grey[300],
                                          fontSize: 16,
                                        ),))
                                      ],
                                    )
                                  ],
                                );
                              });
                            });
                          }
                        },
                        child: const Text(
                          'Register',
                          style: TextStyle(
                            fontStyle: FontStyle.normal,
                            fontSize: 20,
                          ),
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Already with Account?'),
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pushNamedAndRemoveUntil(
                                  '/login/', (route) => false);
                            },
                            child: const Text(' LOGIN'))
                      ],
                    )
                  ],
                ):
                const Center(
                  child: CircularProgressIndicator(),
                )
              ],
            )),
      ),
    );
  }
}
