import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  late String _email;
  late String _password;
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  SharedPreferences? loginData;
  bool? newUser;
  bool hideText = true;
  final _auth = FirebaseAuth.instance;
  String get currUid => FirebaseAuth.instance.currentUser?.uid ?? "";
  Widget _buildEmail() {
    return TextFormField(
      validator: (value) {
        if (value!.isEmpty ||
            !RegExp(r'^[\w-]+@([\w-]+\.)+[\w-]{2,4}').hasMatch(value)) {
          return 'Enter Valid Email';
        } else {
          return null;
        }
      },
      controller: _emailController,
      onSaved: (value) {
        _emailController.text = value!;
      },
      onChanged: (val) {
        setState(() {
          _email = val;
        });
      },
      decoration: InputDecoration(
          labelText: 'Email',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))),
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      onChanged: (val) {
        setState(() {
          _password = val;
        });
      },
      controller: _passwordController,
      decoration: InputDecoration(
          labelText: 'Password',
          suffixIcon: GestureDetector(
            onTap: (){
              setState(() {
                hideText = !hideText;
              });
              FocusScope.of(context).unfocus();
            },
            child: Icon(
              hideText == false?
              Icons.visibility:
                Icons.visibility_off, color: Colors.grey[800],
            ),
          ),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))),
      validator: (value) {
        String missings = '';
        if (value!.length < 8) {
          missings += "Password has at least 8 characters\n";
        }

        if (!RegExp("(?=.*[a-z])").hasMatch(value)) {
          missings += "Password must contain at least one lowercase letter\n";
        }
        if (!RegExp("(?=.*[A-Z])").hasMatch(value)) {
          missings += "Password must contain at least one uppercase letter\n";
        }
        if (!RegExp((r'\d')).hasMatch(value)) {
          missings += "Password must contain at least one digit\n";
        }
        if (!RegExp((r'\W')).hasMatch(value)) {
          missings += "Password must contain at least one symbol\n";
        }

        //if there is password input errors return error string
        if (missings != "") {
          return missings;
        } else {
          return null;
        }
      },
      obscureText: hideText,
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    CheckIfUserIsLoggedIn();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Image(image: AssetImage('asset/jon.jpg')),
              const Image(image: AssetImage('asset/dom.jpg')),
              Container(
                padding: const EdgeInsets.all(20),
                child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            _buildEmail(),
                            const SizedBox(
                              height: 20,
                            ),
                            _buildPassword(),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        ButtonTheme(
                          minWidth: 150,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.teal,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5))),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  String useremail = _emailController.text;
                                  if (useremail != '') {
                                    // ignore: avoid_print
                                    print('Successful');
                                    // ignore: avoid_print
                                    print(currUid);
                                    loginData!
                                        .setString('useremail', useremail);
                                  }
                                  _auth
                                      .signInWithEmailAndPassword(
                                          email: _email,
                                          // ignore: avoid_print
                                          password: _password)
                                      .catchError((err) {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: const Text("Error"),
                                            content: Text("$err"),
                                            actions: [
                                              ElevatedButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: const Text("Ok"))
                                            ],
                                          );
                                        });
                                  }).then((value) {
                                    Navigator.of(context)
                                        .pushNamedAndRemoveUntil(
                                            '/library/', (route) => false);
                                  });
                                  loginData!.setString('useremail', useremail);
                                }
                              },
                              child: Text('LOGIN',
                                  style: TextStyle(
                                      color: Colors.grey[200],
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold))),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/reset/', (route) => false);
                                },
                                child: Text('Forgot Password?',
                                    style: TextStyle(
                                        color: Colors.teal[600],
                                        fontSize: 18))),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Without Account?',
                              style: TextStyle(
                                  color: Colors.grey[800], fontSize: 17),
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/register/', (route) => false);
                                },
                                child: Text('Sign Up',
                                    style: TextStyle(
                                        color: Colors.teal[600], fontSize: 18)))
                          ],
                        )
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  void CheckIfUserIsLoggedIn() async {
    loginData = await SharedPreferences.getInstance();
    newUser = (loginData!.getBool('register') ?? true);
    // ignore: avoid_print
    print(newUser);
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }
}
