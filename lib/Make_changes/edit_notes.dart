import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myapp/fetch/fetch_notes.dart';

class UpdateNote extends StatefulWidget {
  final String id;
  const UpdateNote({Key? key, required this.id}) : super(key: key);

  @override
  State<UpdateNote> createState() => _UpdateNoteState();
}

class _UpdateNoteState extends State<UpdateNote> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference update = FirebaseFirestore.instance.collection('Notes');
  Future<void> bookUpdate(id, note,) async {
    update.doc(id).update({
      'note': note,
    }).then((value) => ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Note Successfully Updated'))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        centerTitle: true,
        title: const Text('UPDATE NOTE'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('Notes')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (!snapshot.hasData) {
                      return const Text('Something Went Wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    var data = snapshot.data!.data();
                    var note = data!['note'];
                    return Column(
                      children: [
                        TextFormField(
                          onChanged: (val) {
                            note = val;
                          },
                          maxLines: 30,
                          initialValue: note,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Note';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Note', border: OutlineInputBorder()),
                        ),
                        const SizedBox(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.teal[900],
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10)
                                    )
                                ),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    bookUpdate(widget.id, note)
                                        .then((value) => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                            const FetchNotes())));
                                  }
                                },
                                child: const Text('Update'))
                          ],
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal[900],
        onPressed: () {},
        child: const Icon(
          Icons.done_all,
          size: 40,
        ),
      ),
    );
  }
}
