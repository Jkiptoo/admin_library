import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EditNote extends StatefulWidget {
  const EditNote({Key? key}) : super(key: key);

  @override
  State<EditNote> createState() => _EditNoteState();
}

class _EditNoteState extends State<EditNote> {
  final Stream<QuerySnapshot> madeNotes =
      FirebaseFirestore.instance.collection("Notes").snapshots();
  final CollectionReference _editNote =
      FirebaseFirestore.instance.collection("Notes");
  bool isLoading = false;
  Future<void> deleteNote(id) async {
    return await _editNote.doc(id).delete().then((value) {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text("Note successfully deleted")));
    }).catchError((error) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Error Message'),
              content: Text(error),
              actions: [
                ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OK"))
              ],
            );
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Edit Note",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: madeNotes,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Error'),
                    content: const Text('Something went wrong'),
                    actions: [
                      ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: const Text("OK"))
                    ],
                  );
                });
          }
          if(snapshot.connectionState == ConnectionState.waiting){
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          final List storeNotes = [];
          snapshot.data!.docs.map((DocumentSnapshot documentSnapshot) {
            Map b = documentSnapshot.data() as Map<String, dynamic>;
            storeNotes.add(b);
            b['id'] = documentSnapshot.id;
          }).toList();
          return  SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
                    children: [
                      for(var i = 0; i< storeNotes.length; i++)...[
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(storeNotes[i]["note"]),
                                ],
                              ),
                              Column(
                                children: [
                                  IconButton(onPressed: (){}, icon:const Icon(Icons.edit, size: 40,)),
                                ],
                              ),
                              Column(
                                children: [
                                  IconButton(onPressed: (){
                                    deleteNote(storeNotes[i]['id']);
                                  }, icon: const Icon(Icons.delete, size: 40,))
                                ],
                              )
                            ],
                          ),
                        )
                      ]
                    ],
                  ),
          );
        },
      ),
    );
  }
}
